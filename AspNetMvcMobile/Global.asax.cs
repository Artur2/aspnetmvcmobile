﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.WebPages;

namespace AspNetMvcMobile
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            DisplayModeProvider.Instance.Modes.Insert(0, new DefaultDisplayMode(DisplayModeProvider.MobileDisplayModeId)
            {
                ContextCondition = c =>
                {
                    Debug.WriteLine("Browser: " + c.Request.Browser.Browser);
                    Debug.WriteLine("IsMobileDevice: " + c.Request.Browser.IsMobileDevice);
                    Debug.WriteLine("Useragent: " + c.Request.UserAgent);
                    return c.Request.Browser.IsMobileDevice;
                }
            });
        }
    }
}
