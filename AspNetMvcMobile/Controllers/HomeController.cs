﻿using System.Web.Mvc;
using System.Web.UI;

namespace AspNetMvcMobile.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [OutputCache(Location = OutputCacheLocation.ServerAndClient, Duration = 60)]
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult AboutUA()
        {
            return View();
        }
    }
}