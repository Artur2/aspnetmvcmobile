

   ----------------------------------------------------------------------
         README file for Web Markup Minifier: Web Extensions 0.8.18

   ----------------------------------------------------------------------

          Copyright 2014 Andrey Taritsyn - http://www.taritsyn.ru
		  
		  
   ===========
   DESCRIPTION
   ===========   
   WebMarkupMin.Web contains 4 HTTP-modules: HtmlMinificationModule 
   (for minification of HTML code), XhtmlMinificationModule (for
   minification of XHTML code), XmlMinificationModule (for minification 
   of XML code) and CompressionModule (for compression of text content
   by using GZIP or Deflate).
   
   HtmlMinificationModule and XhtmlMinificationModule cannot be used 
   together.

   =============
   DOCUMENTATION
   =============
   See more detailed information on CodePlex -
   http://webmarkupmin.codeplex.com